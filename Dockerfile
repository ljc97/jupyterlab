FROM --platform=${TARGETPLATFORM:-linux/amd64} ubuntu:20.04

ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/London"

# tools and dependencies
RUN apt update && apt upgrade -y && apt install -y htop iftop iotop nmon prometheus-node-exporter vnstat net-tools curl
RUN apt install -y python3 python3-pip cmake nodejs npm
RUN npm install -g n
RUN n stable
RUN pip3 install jupyterlab osmium pandas matplotlib ipympl

# jupyter extensions
RUN jupyter labextension install jupyterlab-plotly

# jupyter user
RUN useradd -ms /bin/bash jupyter
USER jupyter
WORKDIR /home/jupyter

# rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/home/jupyter/.cargo/bin:${PATH}"

RUN /home/jupyter/.cargo/bin/cargo install evcxr_jupyter
RUN /home/jupyter/.cargo/bin/evcxr_jupyter --install

CMD ["/bin/bash", "-c", "jupyter lab --ip=0.0.0.0" ]